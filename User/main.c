/*
CTM63_APP_V1.0.0版本
*/
#include "ac780x.h"
#include "app_uart.h"
#include "app_fifo.h"
#include "app_flash.h"
#include "bootLoader.h"
#include "app_rf.h"
#include "app_systick.h"
#include "app_UartPacket.h" 
 
#include "app_wdg.h"
#include "DebugMsg.h"
 

#define BB2W(byteH,byteL) 	(uint16_t)((((byteH)&0x00ff)<<0x0008)|((byteL)&0x00ff))
#define BOFD(data,n)		(uint8_t)((data>>((n)*8))&(0xff))
 
#define APP_MODE						(0xaa)
#define APP_VESION						(0x010000)
#define DEFAULT_ID						(0xf1371008)
#define DEFAULT_PRESSURE				(0)
#define DEFAULT_TEMPERATURE				(0)
#define DEFAULT_STATE					(0)
//#define TEST_ID						(0x200a04b2)
#define SENSOR_COMFIRM_TIMES			(2)		/*故障确认次数*/
#define STUDY_COMFIRM_TIMES				(3)						/*学习次数确认*/
#define STUDY_OUTTIME					(SEC(30))					/*学习超时*/
#define SENSOR_DATA_OUTTIME	 			(MSEC(200))				/*定时上传传感器时间间隔*/
#define UART_SEND_INTERVAL_TIME 		(MSEC(500))              /*发送不同数据包间隔*/
#define UART_RECV_BYTE_INTERVAL_TIME 	(MSEC(3))  	/*接收字节间隔*/
#define SENSOR_NO_SIGNAL_TIME		 	(MINUTE(10))  /*无信号时间*///(MINUTE(10))
#define SEND_REAPET_TIMES 				(10)
#define LOCATION_FRONT					(0x01)
#define LOCATION_REAR					(0x02)
 
typedef enum{NoStudy=0,Studying}studyType;
typedef enum{NoSucceed=0,Succeed}resultType;
typedef enum{NoFrame=0,StudyFrame,FrontFrame,RearFrame}rfFrameType;
typedef struct
{
	BOOL_Type getSiganlflg;
	uint32_t lastSignalTime;
	uint8_t faultNum;
	uint8_t noFaultNum;
	uint8_t lowVoltageNum;
	uint8_t noLowVoltageNum;
}sensorInfoType;


void initSysData(void);
void uartRecv(void);
void uartHandle(void);
void uartSend(void);
void UartTimeout(void);
void RfTask(void);
void studyTask(void);

#pragma pack(4)
struct 
{
	uFlashType* uFlashPtr;
	TimerType rfStudyTimer, uartRecvTimer, uartSendTimer,uartSensorTimer,SecondTimer;
	uint8_t uartSendRepeatTimes,uartCodeRecord;
	uint8_t mode; 
	uint8_t appVersion[3];
	uint8_t handFlg:1,recvUartPacketFlg:1,WriteFlashFlg:1;//app_FlashWriteDataInfo();
 
	uint8_t needSensorDataTimes;
	uint8_t recvUartIndex;
	uint8_t recvUartLength;
	uint8_t sendUartLength;
	sensorInfoType sensorFront,sensorRear;
	uartPacketType packetSendBuf,packetRecvBuf;
	struct
	{
		studyType state;/*学习的状态，0：无学习，1：学习中*/
		resultType result;/*学习结果，0未学习成功，1学习成功*/
		MatchType oldMatch;/*旧的匹配状态*/
		uint8_t   oldSta;/*旧的传感器状态*/
		uint8_t location;/*学习位置*/
		uint8_t times;/*学习次数*/
		uint8_t id[4];/*学习的ID*/
	}rfStudyBuf;
}sGlobal;


int main()
{
	__disable_irq();

	app_tda5235_init();
	
	app_UartInit(19200);//init boudrate 19200s
	
	app_1MsInterruptInit();
	
	initSysData();
	
	app_WDG_Init(2); //WDG_Feed
	
	__enable_irq();
  	 
	DebugMsg("********************************APP************************\r\n");
 
	while(1)
	{
		RfTask();
		uartSend();
		uartRecv();
		uartHandle();/*处理收到的数据包*/
		studyTask();/*处理学习任务*/
		UartTimeout();
	}

}
void initSysData()/*关闭不使用的IO口*/
{
	TimerType* ptr;
	memset(&sGlobal,0,sizeof(sGlobal));
	app_FlashReadDataInfo(); //app_FlashWriteDataInfo();
	sGlobal.uFlashPtr=app_FlashGetDataInfoPtr();
	
	if(sGlobal.uFlashPtr->sData.initialApp!=INITIAL_CONTENT)
	{
		sGlobal.uFlashPtr->sData.initialApp=INITIAL_CONTENT;	
		for(uint8_t ii=0;ii<4;ii++)
		{
			sGlobal.uFlashPtr->sData.ID_Front[ii]=BOFD(DEFAULT_ID,ii);/*写入前轮ID*/
			sGlobal.uFlashPtr->sData.ID_Rear[ii]=BOFD(DEFAULT_ID,ii);/*写入后轮ID*/
		}
//		sGlobal.uFlashPtr->sData.Pressure_Front[0]=BOFD(DEFAULT_PRESSURE,0);
//		sGlobal.uFlashPtr->sData.Pressure_Front[1]=BOFD(DEFAULT_PRESSURE,1);
//		sGlobal.uFlashPtr->sData.Pressure_Rear[0]=BOFD(DEFAULT_PRESSURE,0);
//		sGlobal.uFlashPtr->sData.Pressure_Rear[1]=BOFD(DEFAULT_PRESSURE,1);
//		
//		sGlobal.uFlashPtr->sData.Temperature_Front=BOFD(DEFAULT_TEMPERATURE,0);
//		sGlobal.uFlashPtr->sData.Temperature_Rear=BOFD(DEFAULT_TEMPERATURE,0);
		
		sGlobal.uFlashPtr->sData.Match_Front=NoID;
		sGlobal.uFlashPtr->sData.Match_Rear=NoID;
		
		sGlobal.uFlashPtr->sData.Sta_Front=DEFAULT_STATE;/**/
		sGlobal.uFlashPtr->sData.Sta_Rear=DEFAULT_STATE;	
	}
	sGlobal.uFlashPtr->sData.Pressure_Front[0]=BOFD(DEFAULT_PRESSURE,0);
	sGlobal.uFlashPtr->sData.Pressure_Front[1]=BOFD(DEFAULT_PRESSURE,1);
	sGlobal.uFlashPtr->sData.Pressure_Rear[0]=BOFD(DEFAULT_PRESSURE,0);
	sGlobal.uFlashPtr->sData.Pressure_Rear[1]=BOFD(DEFAULT_PRESSURE,1);
	
	sGlobal.uFlashPtr->sData.Temperature_Front=BOFD(DEFAULT_TEMPERATURE,0);
	sGlobal.uFlashPtr->sData.Temperature_Rear=BOFD(DEFAULT_TEMPERATURE,0);
	
	app_RegisterTimer(&(sGlobal.rfStudyTimer));
	app_RegisterTimer(&(sGlobal.uartRecvTimer));
	app_RegisterTimer(&(sGlobal.uartSendTimer));
	app_RegisterTimer(&(sGlobal.uartSensorTimer));
	app_RegisterTimer(&(sGlobal.SecondTimer));
	for(uint8_t ii=0;ii<3;ii++)					
		sGlobal.appVersion[ii]=BOFD(APP_VESION,ii); 
	sGlobal.mode=APP_MODE;//0x55 is boot, 0xaa is app 
	sGlobal.uFlashPtr->sData.appExitFlg=TRUE;
	app_FlashWriteDataInfo();
	
	sGlobal.uartSendTimer.TimerCount=UART_SEND_INTERVAL_TIME;
}
void UartTimeout()
{
	
	if(sGlobal.needSensorDataTimes==0)
	{
		if(sGlobal.uartSensorTimer.TimerCount>=SENSOR_DATA_OUTTIME)
		{
			sGlobal.uartSensorTimer.TimerCount=0;
			sGlobal.needSensorDataTimes=3; /*定时上传传感器数据*/
		}
	}
	else
	{
		sGlobal.uartSensorTimer.TimerCount=0;
	}
	
	/*无信号监测和复位看门狗*/
	if(sGlobal.SecondTimer.TimerCount>=SEC(1))
	{
		sGlobal.SecondTimer.TimerCount=0;
		WDG_Feed();
		sGlobal.sensorFront.lastSignalTime=sGlobal.sensorFront.lastSignalTime+SEC(1);
		sGlobal.sensorRear.lastSignalTime=sGlobal.sensorRear.lastSignalTime+SEC(1);
	}
	
	if(sGlobal.uFlashPtr->sData.Match_Front==NoID)
	{
		
		if(sGlobal.uFlashPtr->sData.ID_Front[3]!=BOFD(DEFAULT_ID,3)||
			sGlobal.uFlashPtr->sData.ID_Front[2]!=BOFD(DEFAULT_ID,2)||
			sGlobal.uFlashPtr->sData.ID_Front[1]!=BOFD(DEFAULT_ID,1)||
			sGlobal.uFlashPtr->sData.ID_Front[0]!=BOFD(DEFAULT_ID,0) )
		{
			sGlobal.uFlashPtr->sData.Match_Front=HaveID;
		}
		else
		{
			sGlobal.uFlashPtr->sData.Sta_Front=DEFAULT_STATE;/*未匹配无故障信息*/
			sGlobal.sensorFront.getSiganlflg=TRUE;
		}
	}
	if(sGlobal.uFlashPtr->sData.Match_Rear==NoID)
	{
		
		if(sGlobal.uFlashPtr->sData.ID_Rear[3]!=BOFD(DEFAULT_ID,3)||
			sGlobal.uFlashPtr->sData.ID_Rear[2]!=BOFD(DEFAULT_ID,2)||
			sGlobal.uFlashPtr->sData.ID_Rear[1]!=BOFD(DEFAULT_ID,1)||
			sGlobal.uFlashPtr->sData.ID_Rear[0]!=BOFD(DEFAULT_ID,0) )
		{
			sGlobal.uFlashPtr->sData.Match_Rear=HaveID;
		}
		else
		{
			sGlobal.uFlashPtr->sData.Sta_Rear=DEFAULT_STATE;/*未匹配无故障信息*/
			sGlobal.sensorRear.getSiganlflg=TRUE;
		}
	}
	
	if(sGlobal.uFlashPtr->sData.Match_Front==HaveID)
	{
		if(STATE_GET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Front)==0)
		{
			if(sGlobal.sensorFront.lastSignalTime>=SENSOR_NO_SIGNAL_TIME)
				STATE_SET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Front);/*置位无信号*/
		}
	}
	if(sGlobal.uFlashPtr->sData.Match_Rear==HaveID)
	{
		if(STATE_GET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Rear)==0)
		{
			if(sGlobal.sensorRear.lastSignalTime>=SENSOR_NO_SIGNAL_TIME)
				STATE_SET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Rear);/*置位无信号*/
		}
	}
	/*收到信号或未匹配*/
	if(sGlobal.sensorFront.getSiganlflg==TRUE)
	{
		sGlobal.sensorFront.getSiganlflg=FALSE;
		sGlobal.sensorFront.lastSignalTime=0;
		STATE_CLEAR_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Front);/*信号正常*/
	}
	if(sGlobal.sensorRear.getSiganlflg==TRUE)
	{
		sGlobal.sensorRear.getSiganlflg=FALSE;
		sGlobal.sensorRear.lastSignalTime=0;
		STATE_CLEAR_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Rear);/*信号正常*/
	}
	if(sGlobal.WriteFlashFlg==TRUE)
	{
		sGlobal.WriteFlashFlg=FALSE;
		app_FlashWriteDataInfo();
		DebugMsg("/*******************************写入数据到FLASH中*************************************/\r\n");
	}
}
/*发送数据包*/
void uartSend()
{
	if(sGlobal.uartSendTimer.TimerCount>=UART_SEND_INTERVAL_TIME)
	{
		if(sGlobal.handFlg==FALSE)/*发送握手指令*/
		{
			sGlobal.packetSendBuf=getHandCmd(0x01,sGlobal.mode);
			app_sendUartData((sGlobal.packetSendBuf.dataBuf),sGlobal.packetSendBuf.dataPacket.dlc);
		}
		else if(sGlobal.needSensorDataTimes>0)/*发送传感器数据*/
		{
			/*数据保存在FLASH的指针中*/
			uint8_t frontStudyState=sGlobal.uFlashPtr->sData.Match_Front;
			uint8_t rearStudyState=sGlobal.uFlashPtr->sData.Match_Rear;
            sGlobal.packetSendBuf=getSensorData(sGlobal.uFlashPtr->sData.Pressure_Front[1],sGlobal.uFlashPtr->sData.Pressure_Front[0],
													sGlobal.uFlashPtr->sData.Temperature_Front,sGlobal.uFlashPtr->sData.Sta_Front,
													sGlobal.uFlashPtr->sData.ID_Front[3],sGlobal.uFlashPtr->sData.ID_Front[2],sGlobal.uFlashPtr->sData.ID_Front[1],sGlobal.uFlashPtr->sData.ID_Front[0],
													frontStudyState,
													sGlobal.uFlashPtr->sData.Pressure_Rear[1],sGlobal.uFlashPtr->sData.Pressure_Rear[0],
													sGlobal.uFlashPtr->sData.Temperature_Rear,sGlobal.uFlashPtr->sData.Sta_Rear,
													sGlobal.uFlashPtr->sData.ID_Rear[3],sGlobal.uFlashPtr->sData.ID_Rear[2],sGlobal.uFlashPtr->sData.ID_Rear[1],sGlobal.uFlashPtr->sData.ID_Rear[0],
													rearStudyState,
													sGlobal.appVersion[2],sGlobal.appVersion[1],sGlobal.appVersion[0]);	
			do{
				sGlobal.needSensorDataTimes--;
				app_sendUartData((sGlobal.packetSendBuf.dataBuf),sGlobal.packetSendBuf.dataPacket.dlc);
			}while(sGlobal.needSensorDataTimes!=0);
	 	
			DebugMsg("\r\n");
			DebugMsg("前轮ID[%x,%x,%x,%x]\r\n",sGlobal.uFlashPtr->sData.ID_Front[3],sGlobal.uFlashPtr->sData.ID_Front[2],sGlobal.uFlashPtr->sData.ID_Front[1],sGlobal.uFlashPtr->sData.ID_Front[0]);
			DebugMsg("前轮匹配状态[%s]\r\n",(frontStudyState==0?"未匹配":frontStudyState==1?"学习中":frontStudyState==2?"已匹配":"匹配状态错误"));
			DebugMsg("[%s]\r\n,",STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Front)?"存在故障":"状态正常");
			DebugMsg("[%s]\r\n,",STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Front)?"低电压":"电压正常");
			DebugMsg("[%s]\r\n",STATE_GET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Front)?"没有信号":"信号正常");
			DebugMsg("前轮温度%d\r\n",sGlobal.uFlashPtr->sData.Temperature_Front-50);
			DebugMsg("前轮气压%d\r\n",BB2W(sGlobal.uFlashPtr->sData.Pressure_Front[1],sGlobal.uFlashPtr->sData.Pressure_Front[0]));
			DebugMsg("\r\n");
			DebugMsg("后轮ID[%x,%x,%x,%x]\r\n",sGlobal.uFlashPtr->sData.ID_Rear[3],sGlobal.uFlashPtr->sData.ID_Rear[2],sGlobal.uFlashPtr->sData.ID_Rear[1],sGlobal.uFlashPtr->sData.ID_Rear[0]);
			DebugMsg("后轮匹配状态[%s]\r\n",(rearStudyState==0?"未匹配":rearStudyState==1?"学习中":rearStudyState==2?"已匹配":"匹配状态错误"));
			DebugMsg("[%s]\r\n",STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Rear)?"存在故障":"状态正常");
			DebugMsg("[%s]\r\n",STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Rear)?"低电压":"电压正常");
			DebugMsg("[%s]\r\n",STATE_GET_SIGNAL(sGlobal.uFlashPtr->sData.Sta_Rear)?"没有信号":"信号正常");
			DebugMsg("后轮温度%d\r\n",sGlobal.uFlashPtr->sData.Temperature_Rear-50);
			DebugMsg("后轮气压%d\r\n",BB2W(sGlobal.uFlashPtr->sData.Pressure_Rear[1],sGlobal.uFlashPtr->sData.Pressure_Rear[0]));
			 
		}
		sGlobal.uartSendTimer.TimerCount=0;	
	}	
}
/*接收数据包*/	
void uartRecv()
{
	if(sGlobal.uartRecvTimer.TimerCount>UART_RECV_BYTE_INTERVAL_TIME)
	{
		if(app_getLenOfUartData()==0)
		{
			sGlobal.recvUartIndex=0; 
		}
		sGlobal.uartRecvTimer.TimerCount=0;
	}
	while(app_getLenOfUartData()>0&&sGlobal.recvUartPacketFlg==FALSE)
	{
		sGlobal.uartRecvTimer.TimerCount=0;
		uint8_t data = app_getUartByte();
		switch(sGlobal.recvUartIndex){
		case 0:
			if(data==0x55)
				sGlobal.packetRecvBuf.dataBuf[sGlobal.recvUartIndex++]=0x55;
			break;
		case 1:
			if(data==0xaa)
				sGlobal.packetRecvBuf.dataBuf[sGlobal.recvUartIndex++]=0xaa;
			else
				sGlobal.recvUartIndex=0;
			break;
		case 2:
			if(data>5) //dlc
				sGlobal.packetRecvBuf.dataBuf[sGlobal.recvUartIndex++]=data;
			else
				sGlobal.recvUartIndex=0;
			break;
		case 3:
			if(data<4) //action
				sGlobal.packetRecvBuf.dataBuf[sGlobal.recvUartIndex++]=data;
			else
				sGlobal.recvUartIndex=0;
			break;
		default://
			sGlobal.packetRecvBuf.dataBuf[sGlobal.recvUartIndex++]=data;
			if(sGlobal.recvUartIndex==sGlobal.packetRecvBuf.dataPacket.dlc)
			{
				sGlobal.recvUartIndex=0;
				BOOL_Type right=isUartPacket(&sGlobal.packetRecvBuf); /*checkData is right*/
				if(right==TRUE)
				{
					sGlobal.recvUartPacketFlg=TRUE;//sGlobal.recvUartLength=sGlobal.packetRecvBuf.dataPacket.dlc;
					//break;
				}
			}
		}
		
	}
}
void uartHandle(void)
{
	/*handle uart recv data*/
	if(sGlobal.recvUartPacketFlg==TRUE)
	{
		sGlobal.recvUartPacketFlg=FALSE;
		uint8_t action=packetAction(sGlobal.packetRecvBuf);
		unionPlayLoad playLoad=packetPlayLoad(sGlobal.packetRecvBuf);
		sGlobal.uartCodeRecord=packetCode(sGlobal.packetRecvBuf);
		//sGlobal.uartSendRepeatTimes=0;
		sGlobal.uartSendTimer.TimerCount=UART_SEND_INTERVAL_TIME;
		
		switch(sGlobal.uartCodeRecord)
		{
			case 0x00:/*response handshake or app update*/
				if(playLoad.handCmd.mode==0x00)
				{
					sGlobal.handFlg=TRUE; /*收到握手指令*/
					if(action!=0)
					{
						NVIC_SystemReset();/*有升级请求重启进入Boot*/
					}
					else
					{
						sGlobal.needSensorDataTimes=3;
					}
				}
			break;
			case 0x04:/*response id study*/
				if(sGlobal.rfStudyBuf.state==NoStudy&&sGlobal.handFlg==TRUE)
				{
					sGlobal.rfStudyBuf.state=Studying;
					sGlobal.rfStudyBuf.result=NoSucceed;
					sGlobal.rfStudyBuf.times=0;
					sGlobal.rfStudyBuf.location=playLoad.studyCmd.location;
					sGlobal.rfStudyTimer.TimerCount=0;
//					DebugMsg("\r\n");
//					DebugMsg("进入学习[%x]\r\n",sGlobal.rfStudyBuf.location);
					if(sGlobal.rfStudyBuf.location==LOCATION_FRONT)
					{
						sGlobal.rfStudyBuf.oldMatch=sGlobal.uFlashPtr->sData.Match_Front; /*记录之前的匹配状态*/
						sGlobal.rfStudyBuf.oldSta=sGlobal.uFlashPtr->sData.Sta_Front;/*旧的故障信息*/
						sGlobal.uFlashPtr->sData.Sta_Front=DEFAULT_STATE;/**/
						sGlobal.uFlashPtr->sData.Match_Front=StudyID;
						
					}
					else if(sGlobal.rfStudyBuf.location==LOCATION_REAR)
					{
						sGlobal.rfStudyBuf.oldMatch=sGlobal.uFlashPtr->sData.Match_Rear;/**/
						sGlobal.rfStudyBuf.oldSta=sGlobal.uFlashPtr->sData.Sta_Rear;
						sGlobal.uFlashPtr->sData.Sta_Rear=DEFAULT_STATE;/**/
						sGlobal.uFlashPtr->sData.Match_Rear=StudyID;
					}
				}
				sGlobal.needSensorDataTimes=3;
				/*连续发送传感器数据3次*/
			break;
			case 0x08:/*response exit study id*/
				if(sGlobal.rfStudyBuf.state==Studying)
				{
					sGlobal.rfStudyBuf.state=NoStudy;
					sGlobal.rfStudyBuf.result=NoSucceed;
					if(sGlobal.rfStudyBuf.location==LOCATION_FRONT)
					{
						sGlobal.uFlashPtr->sData.Match_Front=sGlobal.rfStudyBuf.oldMatch;/*退出学习恢复旧的状态*/
						sGlobal.uFlashPtr->sData.Sta_Front=sGlobal.rfStudyBuf.oldSta;/*恢复旧的传感器故障信息*/
					}
					else if(sGlobal.rfStudyBuf.location==LOCATION_REAR)
					{
						sGlobal.uFlashPtr->sData.Match_Rear=sGlobal.rfStudyBuf.oldMatch;
						sGlobal.uFlashPtr->sData.Sta_Rear=sGlobal.rfStudyBuf.oldSta;
					}
				}
				sGlobal.needSensorDataTimes=3;
			break;
		}
			
	}
	
}
void studyTask(void)
{
	/*handle study id*/
	if(sGlobal.rfStudyBuf.state==Studying)
	{
		if(sGlobal.rfStudyBuf.result==Succeed)
		{
			/*学习成功处理*/
			uint8_t ii=0;
			switch(sGlobal.rfStudyBuf.location)
			{
				case 0x01:
					for(ii=0;ii<4;ii++)
						sGlobal.uFlashPtr->sData.ID_Front[ii]=sGlobal.rfStudyBuf.id[ii];/*写入前轮ID*/
					sGlobal.uFlashPtr->sData.Match_Front=HaveID;
					sGlobal.sensorFront.getSiganlflg=TRUE;
					sGlobal.uFlashPtr->sData.Sta_Front=	DEFAULT_STATE;
					sGlobal.uFlashPtr->sData.Pressure_Front[0]=BOFD(DEFAULT_PRESSURE,0);
					sGlobal.uFlashPtr->sData.Pressure_Front[1]=BOFD(DEFAULT_PRESSURE,1);
					sGlobal.uFlashPtr->sData.Temperature_Front=DEFAULT_TEMPERATURE;
					DebugMsg("\r\n");
					DebugMsg("写入前轮ID信息[%x,%X,%x,%x]\r\n",sGlobal.rfStudyBuf.id[3],sGlobal.rfStudyBuf.id[2],sGlobal.rfStudyBuf.id[1],sGlobal.rfStudyBuf.id[0]);
				break;
				case 0x02:
					for(ii=0;ii<4;ii++)
						sGlobal.uFlashPtr->sData.ID_Rear[ii]=sGlobal.rfStudyBuf.id[ii];/*写入后轮ID*/
					sGlobal.sensorRear.getSiganlflg=TRUE;
					sGlobal.uFlashPtr->sData.Match_Rear=HaveID;
					sGlobal.uFlashPtr->sData.Sta_Rear=	DEFAULT_STATE;
					sGlobal.uFlashPtr->sData.Pressure_Rear[0]=BOFD(DEFAULT_PRESSURE,0);
					sGlobal.uFlashPtr->sData.Pressure_Rear[1]=BOFD(DEFAULT_PRESSURE,1);
					sGlobal.uFlashPtr->sData.Temperature_Rear=DEFAULT_TEMPERATURE;
					DebugMsg("\r\n");
					DebugMsg("写入后轮ID信息[%x,%X,%x,%x]\r\n",sGlobal.rfStudyBuf.id[3],sGlobal.rfStudyBuf.id[2],sGlobal.rfStudyBuf.id[1],sGlobal.rfStudyBuf.id[0]);
				break;
			}
			
			for(ii=0;ii<4;ii++)
			{
				if(sGlobal.uFlashPtr->sData.ID_Front[ii]!=sGlobal.uFlashPtr->sData.ID_Rear[ii])/*判断前后轮ID是否相同*/
					break;
			}
			if(ii==4)
			{
				/*前后轮ID相同, 擦除未学习轮位ID*/
				switch(sGlobal.rfStudyBuf.location)
				{
					case 0x01:
						for(ii=0;ii<4;ii++)
							sGlobal.uFlashPtr->sData.ID_Rear[ii]=BOFD(DEFAULT_ID,ii) ;/*写入前轮默认ID*/
						sGlobal.uFlashPtr->sData.Match_Rear=NoID;
						sGlobal.uFlashPtr->sData.Sta_Rear=	DEFAULT_STATE;
						sGlobal.uFlashPtr->sData.Pressure_Rear[0]=BOFD(DEFAULT_PRESSURE,0);
						sGlobal.uFlashPtr->sData.Pressure_Rear[1]=BOFD(DEFAULT_PRESSURE,1);
						sGlobal.uFlashPtr->sData.Temperature_Rear=DEFAULT_TEMPERATURE;
						DebugMsg("\r\n");
						DebugMsg("擦除后轮信息\r\n");
					break;
					case 0x02:
						for(ii=0;ii<4;ii++)
							sGlobal.uFlashPtr->sData.ID_Front[ii]= BOFD(DEFAULT_ID,ii);/*写入后轮默认ID*/
						sGlobal.uFlashPtr->sData.Match_Front=NoID;
						sGlobal.uFlashPtr->sData.Sta_Front=	DEFAULT_STATE;
						sGlobal.uFlashPtr->sData.Pressure_Front[0]=BOFD(DEFAULT_PRESSURE,0);
						sGlobal.uFlashPtr->sData.Pressure_Front[1]=BOFD(DEFAULT_PRESSURE,1);
						sGlobal.uFlashPtr->sData.Temperature_Front=DEFAULT_TEMPERATURE;
						DebugMsg("\r\n");
						DebugMsg("擦除前轮信息\r\n");
					break;
				}
			}
			sGlobal.rfStudyBuf.state=NoStudy;
			sGlobal.rfStudyBuf.result=NoSucceed;
			sGlobal.needSensorDataTimes=3;
			sGlobal.WriteFlashFlg=TRUE;/*写Flash*/
			DebugMsg("\r\n");
			DebugMsg("学习成功！位置:%d \r\n",sGlobal.rfStudyBuf.location);
		}
		else 
		{
			/*学习超时*/
			if(sGlobal.rfStudyTimer.TimerCount>STUDY_OUTTIME)
			{
				sGlobal.rfStudyTimer.TimerCount=0;
				sGlobal.rfStudyBuf.state=NoStudy;
				sGlobal.rfStudyBuf.result=NoSucceed;
				if(sGlobal.rfStudyBuf.location==LOCATION_FRONT)
				{
						sGlobal.uFlashPtr->sData.Match_Front=sGlobal.rfStudyBuf.oldMatch;/*退出学习恢复旧的状态*/
				}
				else if(sGlobal.rfStudyBuf.location==LOCATION_REAR)
				{
					sGlobal.uFlashPtr->sData.Match_Rear=sGlobal.rfStudyBuf.oldMatch;/**/
				}
				sGlobal.needSensorDataTimes=3;
				DebugMsg("\r\n");
				DebugMsg("学习超时！位置:%d\r\n",sGlobal.rfStudyBuf.location);
			}
			
		}
			
	}
}
void RfTask()
{
	static BOOL_Type getRfDataFlg=FALSE;
	static rfFrameType DataType=NoFrame;
	static rfDataType rfData={0};
	if(Bsp_TDA5235_IsDataRdy())
	{
		uint8_t dataBuf[6];
		Bsp_TDA5235_ReadFifoData(dataBuf,6);
		if(app_IsValidData(dataBuf))
		{
			DataType=NoFrame;
			if(COM_vMsgFrameCRC8(sGlobal.uFlashPtr->sData.ID_Front,dataBuf))
			{
				/*前轮数据帧*/
				DataType=FrontFrame;
				getRfDataFlg=TRUE;
			}
			if(COM_vMsgFrameCRC8(sGlobal.uFlashPtr->sData.ID_Rear,dataBuf))
			{
				/*后轮数据帧*/
				DataType=RearFrame;
				getRfDataFlg=TRUE;
			}
			if(COM_vStudyFrameCRC8(dataBuf))
			{
				/*学习帧*/
				DataType=StudyFrame;
				getRfDataFlg=TRUE;
			}
			if(getRfDataFlg==TRUE)
			{
				/*赋值Rf数据到buf*/
				for(uint8_t ii=0;ii<6;ii++)
					rfData.rfBuf[ii]=dataBuf[ii];
			}
		}
	}
	if(getRfDataFlg==TRUE)
	{
		getRfDataFlg=FALSE;
		switch(DataType)
		{
			case StudyFrame:
				if(sGlobal.rfStudyBuf.state==Studying&&sGlobal.rfStudyBuf.result==NoSucceed)
				{
					if(sGlobal.rfStudyBuf.location==rfData.studyFrame.Tp)
					{
						if(sGlobal.rfStudyBuf.id[0]!=rfData.studyFrame.ID0||
							sGlobal.rfStudyBuf.id[1]!=rfData.studyFrame.ID1||
							sGlobal.rfStudyBuf.id[2]!=rfData.studyFrame.ID2||
							sGlobal.rfStudyBuf.id[3]!=rfData.studyFrame.ID3)
						{
							sGlobal.rfStudyBuf.id[0]=rfData.studyFrame.ID0;
							sGlobal.rfStudyBuf.id[1]=rfData.studyFrame.ID1;
							sGlobal.rfStudyBuf.id[2]=rfData.studyFrame.ID2;
							sGlobal.rfStudyBuf.id[3]=rfData.studyFrame.ID3;
							sGlobal.rfStudyBuf.times=0;
						}
						sGlobal.rfStudyBuf.times++;
						if(sGlobal.rfStudyBuf.times>=STUDY_COMFIRM_TIMES)
						{
							sGlobal.rfStudyBuf.result=Succeed;/*学习成功*/
							sGlobal.rfStudyBuf.oldMatch=HaveID;
							sGlobal.rfStudyBuf.oldSta=DEFAULT_STATE;
						}
					}
				}
				DebugMsg("收到RF学习帧数据:%d\r\n",rfData.studyFrame.Tp);
				DebugMsg("ID:%x %x %x %x\r\n",rfData.studyFrame.ID0,rfData.studyFrame.ID1,rfData.studyFrame.ID2,rfData.studyFrame.ID3);
				break;
			case FrontFrame:
				sGlobal.sensorFront.getSiganlflg=TRUE;/*置位获取到前轮信号*/
				sGlobal.uFlashPtr->sData.Pressure_Front[0]=rfData.dataFrame.PLval;
				sGlobal.uFlashPtr->sData.Pressure_Front[1]=rfData.dataFrame.PHval;
				sGlobal.uFlashPtr->sData.Temperature_Front=rfData.dataFrame.Tval;
				sGlobal.uFlashPtr->sData.FVal_Front=rfData.dataFrame.Fval;
				if(sGlobal.uFlashPtr->sData.FVal_Front==SENSOR_FAULT)
				{
					sGlobal.sensorFront.noFaultNum=0;
					if(STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Front)==BitFalse)
					{
						if(sGlobal.sensorFront.faultNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorFront.faultNum++;
							if(sGlobal.sensorFront.faultNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_SET_FAULT(sGlobal.uFlashPtr->sData.Sta_Front);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
						
					}
				}
				else
				{
					sGlobal.sensorFront.faultNum=0; 
					if(STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Front)==BitTrue)
					{
						if(sGlobal.sensorFront.noFaultNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorFront.noFaultNum++;
							if(sGlobal.sensorFront.noFaultNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_CLEAR_FAULT(sGlobal.uFlashPtr->sData.Sta_Front);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				if(rfData.dataFrame.Sval&B010)
				{
					sGlobal.sensorFront.noLowVoltageNum=0;
					if(STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Front)==BitFalse)
					{
						if(sGlobal.sensorFront.lowVoltageNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorFront.lowVoltageNum++;
							if(sGlobal.sensorFront.lowVoltageNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_SET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Front);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				else
				{
					sGlobal.sensorFront.lowVoltageNum=0;
					if(STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Front)==BitTrue)
					{
						if(sGlobal.sensorFront.noLowVoltageNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorFront.noLowVoltageNum++;
							if(sGlobal.sensorFront.noLowVoltageNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_CLEAR_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Front);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				DebugMsg("收到RF前轮数据帧,温度%d\r\n",rfData.dataFrame.Tval-50);
				break; 
			case RearFrame:
				sGlobal.sensorRear.getSiganlflg=TRUE;
				sGlobal.uFlashPtr->sData.Pressure_Rear[0]=rfData.dataFrame.PLval;
				sGlobal.uFlashPtr->sData.Pressure_Rear[1]=rfData.dataFrame.PHval;
				sGlobal.uFlashPtr->sData.Temperature_Rear=rfData.dataFrame.Tval;
				sGlobal.uFlashPtr->sData.FVal_Rear=rfData.dataFrame.Fval;
				if(sGlobal.uFlashPtr->sData.FVal_Rear==SENSOR_FAULT)
				{
					sGlobal.sensorRear.noFaultNum=0;
					if(STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Rear)==BitFalse)
					{
						if(sGlobal.sensorRear.faultNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorRear.faultNum++;
							if(sGlobal.sensorRear.faultNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_SET_FAULT(sGlobal.uFlashPtr->sData.Sta_Rear);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				else
				{
					sGlobal.sensorRear.faultNum=0; 
					if(STATE_GET_FAULT(sGlobal.uFlashPtr->sData.Sta_Rear)==BitTrue)
					{
						if(sGlobal.sensorRear.noFaultNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorRear.noFaultNum++;
							if(sGlobal.sensorRear.noFaultNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_CLEAR_FAULT(sGlobal.uFlashPtr->sData.Sta_Rear);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				
				if(rfData.dataFrame.Sval&B010)
				{
					sGlobal.sensorRear.noLowVoltageNum=0;
					if(STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Rear)==BitFalse)
					{
						if(sGlobal.sensorRear.lowVoltageNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorRear.lowVoltageNum++;
							if(sGlobal.sensorRear.lowVoltageNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_SET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Rear);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				else
				{
					sGlobal.sensorRear.lowVoltageNum=0;
					if(STATE_GET_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Rear)==BitTrue)
					{
						if(sGlobal.sensorRear.noLowVoltageNum<SENSOR_COMFIRM_TIMES)
						{
							sGlobal.sensorRear.noLowVoltageNum++;
							if(sGlobal.sensorRear.noLowVoltageNum==SENSOR_COMFIRM_TIMES)
							{
								STATE_CLEAR_VOLTAGE(sGlobal.uFlashPtr->sData.Sta_Rear);
								sGlobal.WriteFlashFlg=TRUE;
							}
						}
					}
				}
				
				DebugMsg("收到RF后轮数据帧,温度%d\r\n",rfData.dataFrame.Tval-50);
				break;
			case NoFrame:
			default:
				DebugMsg("不是正确是rf帧格式\r\n");
				while(1);
		}
	}
}
 


